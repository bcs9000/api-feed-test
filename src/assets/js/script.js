const app = document.getElementById('feed')

const wrapper = document.createElement('div')
wrapper.setAttribute('class', 'wrapper')

app.appendChild(wrapper)

var request = new XMLHttpRequest()
request.open('GET', 'https://pokeapi.co/api/v2/pokemon/?limit=151&offset=0', true)
request.onload = function() {
  // Begin accessing JSON data here
  var data = JSON.parse(this.response)
  if (request.status >= 200 && request.status < 400) {
    console.log(data.results);
    data.results.forEach(item => {
      const card = document.createElement('div')
      card.setAttribute('class', 'item')
      card.setAttribute('style', '--aspect-ratio: 1/1;')

      const a = document.createElement('a');
      a.innerHTML = item.name;
      a.setAttribute('title', item.name);
      a.setAttribute('href', item.url);
      wrapper.appendChild(card)
      card.appendChild(a)
    })
  } else {
    const errorMessage = document.createElement('div')
    errorMessage.textContent = 'Nenhum dado carregado :('
    app.appendChild(errorMessage)
  }
}

request.send()