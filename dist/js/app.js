const app = document.getElementById('feed')

const wrapper = document.createElement('div')
wrapper.setAttribute('class', 'wrapper')

app.appendChild(wrapper)

var request = new XMLHttpRequest()
request.open('GET', 'https://pokeapi.co/api/v2/pokemon/?limit=151&offset=0', true)
request.onload = function() {
  // Begin accessing JSON data here
  var data = JSON.parse(this.response)
  if (request.status >= 200 && request.status < 400) {
    console.log(data.results);
    data.results.forEach(item => {
      const card = document.createElement('div')
      card.setAttribute('class', 'item')
      card.setAttribute('style', '--aspect-ratio: 1/1;')

      const a = document.createElement('a');
      a.innerHTML = item.name;
      a.setAttribute('title', item.name);
      a.setAttribute('href', item.url);
      wrapper.appendChild(card)
      card.appendChild(a)
    })
  } else {
    const errorMessage = document.createElement('div')
    errorMessage.textContent = 'Nenhum dado carregado :('
    app.appendChild(errorMessage)
  }
}

request.send()
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjcmlwdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBhcHAgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZmVlZCcpXHJcblxyXG5jb25zdCB3cmFwcGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jylcclxud3JhcHBlci5zZXRBdHRyaWJ1dGUoJ2NsYXNzJywgJ3dyYXBwZXInKVxyXG5cclxuYXBwLmFwcGVuZENoaWxkKHdyYXBwZXIpXHJcblxyXG52YXIgcmVxdWVzdCA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpXHJcbnJlcXVlc3Qub3BlbignR0VUJywgJ2h0dHBzOi8vcG9rZWFwaS5jby9hcGkvdjIvcG9rZW1vbi8/bGltaXQ9MTUxJm9mZnNldD0wJywgdHJ1ZSlcclxucmVxdWVzdC5vbmxvYWQgPSBmdW5jdGlvbigpIHtcclxuICAvLyBCZWdpbiBhY2Nlc3NpbmcgSlNPTiBkYXRhIGhlcmVcclxuICB2YXIgZGF0YSA9IEpTT04ucGFyc2UodGhpcy5yZXNwb25zZSlcclxuICBpZiAocmVxdWVzdC5zdGF0dXMgPj0gMjAwICYmIHJlcXVlc3Quc3RhdHVzIDwgNDAwKSB7XHJcbiAgICBjb25zb2xlLmxvZyhkYXRhLnJlc3VsdHMpO1xyXG4gICAgZGF0YS5yZXN1bHRzLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgIGNvbnN0IGNhcmQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxyXG4gICAgICBjYXJkLnNldEF0dHJpYnV0ZSgnY2xhc3MnLCAnaXRlbScpXHJcbiAgICAgIGNhcmQuc2V0QXR0cmlidXRlKCdzdHlsZScsICctLWFzcGVjdC1yYXRpbzogMS8xOycpXHJcblxyXG4gICAgICBjb25zdCBhID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xyXG4gICAgICBhLmlubmVySFRNTCA9IGl0ZW0ubmFtZTtcclxuICAgICAgYS5zZXRBdHRyaWJ1dGUoJ3RpdGxlJywgaXRlbS5uYW1lKTtcclxuICAgICAgYS5zZXRBdHRyaWJ1dGUoJ2hyZWYnLCBpdGVtLnVybCk7XHJcbiAgICAgIHdyYXBwZXIuYXBwZW5kQ2hpbGQoY2FyZClcclxuICAgICAgY2FyZC5hcHBlbmRDaGlsZChhKVxyXG4gICAgfSlcclxuICB9IGVsc2Uge1xyXG4gICAgY29uc3QgZXJyb3JNZXNzYWdlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcclxuICAgIGVycm9yTWVzc2FnZS50ZXh0Q29udGVudCA9ICdOZW5odW0gZGFkbyBjYXJyZWdhZG8gOignXHJcbiAgICBhcHAuYXBwZW5kQ2hpbGQoZXJyb3JNZXNzYWdlKVxyXG4gIH1cclxufVxyXG5cclxucmVxdWVzdC5zZW5kKCkiXX0=
